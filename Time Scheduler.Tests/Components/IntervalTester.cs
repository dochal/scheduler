﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Time_Scheduler.Components;

namespace Time_Scheduler.Tests.Components
{
    [TestClass]
    public class IntervalTester
    {
        [TestMethod]
        public void InInverval()
        {
            Interval i = new Interval(new DateTime(2015,11,11,12,30,00), new DateTime(2015,11,11,19,30,00));

            Assert.IsTrue(i.InInterval(new DateTime(2015, 11, 11, 12, 30, 00)));
            Assert.IsTrue(i.InInterval(new DateTime(2015, 11, 11, 12, 31, 00)));
            Assert.IsFalse(i.InInterval(new DateTime(2015, 11, 11, 12, 29, 00)));

            Assert.IsTrue(i.InInterval(new DateTime(2015, 11, 11, 19, 29, 00)));
            Assert.IsTrue(i.InInterval(new DateTime(2015, 11, 11, 19, 30, 00)));
            Assert.IsFalse(i.InInterval(new DateTime(2015, 11, 11, 19, 31, 00)));
        }

        [TestMethod]
        public void Fits()
        {
            Interval i = new Interval(new DateTime(2015, 11, 11, 12, 30, 00), new DateTime(2015, 11, 11, 19, 30, 00));

            Assert.IsTrue(i.Fits(new Interval(new DateTime(2015, 11, 11, 12, 30, 00), new DateTime(2015, 11, 11, 19, 30, 00))));
            Assert.IsTrue(i.Fits(new Interval(new DateTime(2015, 11, 11, 12, 30, 00), new DateTime(2015, 11, 11, 19, 29, 00))));
            Assert.IsFalse(i.Fits(new Interval(new DateTime(2015, 11, 11, 12, 30, 00), new DateTime(2015, 11, 11, 19, 31, 00))));

            Assert.IsTrue(i.Fits(new Interval(new DateTime(2015, 11, 11, 12, 31, 00), new DateTime(2015, 11, 11, 19, 30, 00))));
            Assert.IsTrue(i.Fits(new Interval(new DateTime(2015, 11, 11, 12, 30, 00), new DateTime(2015, 11, 11, 19, 30, 00))));
            Assert.IsFalse(i.Fits(new Interval(new DateTime(2015, 11, 11, 12, 29, 00), new DateTime(2015, 11, 11, 19, 30, 00))));
        }

        [TestMethod]
        public void IntervalComparator()
        {
            
        }

    }
}
