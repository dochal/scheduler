﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Time_Scheduler.Components;

namespace Time_Scheduler.Tests.Components
{
    [TestClass]
    public class RangeTester
    {

        [TestMethod]
        public void InRangeChecks()
        {
            Time_Scheduler.Components.Range r = new Time_Scheduler.Components.Range(TimeSpan.FromMinutes(12), TimeSpan.FromMinutes(24));

            Assert.IsTrue(r.InRange(TimeSpan.FromMinutes(12)));
            Assert.IsTrue(r.InRange(TimeSpan.FromMinutes(24)));
            Assert.IsTrue(r.InRange(TimeSpan.FromMinutes(20)));
            Assert.IsFalse(r.InRange(TimeSpan.FromMinutes(11)));
            Assert.IsFalse(r.InRange(TimeSpan.FromMinutes(8)));
            Assert.IsFalse(r.InRange(TimeSpan.FromMinutes(25)));
            Assert.IsFalse(r.InRange(TimeSpan.FromMinutes(28)));
        }

    }
}
