﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Time_Scheduler.Algorithms;
using Time_Scheduler.Models;
using Time_Scheduler.Models.Items;
using Time_Scheduler.Models.Travel;

namespace Time_Scheduler.Tests.Algorithms
{
    [TestClass]
    public class AlgorithmTester
    {
        [TestMethod]
        public void PlanGenerator()
        {
            AlgorithmStub s = new AlgorithmStub(new List<Activity>(), new Map());

            LinkedList<Activity> plan = s.PlanTemplate();
            if(plan.Count != 2)
            {
                Assert.Fail();
            }

        }

        [TestMethod]
        public void AddActivity()
        { 
            Map m = new Map();
            m.InitialUser = new Location("Hello", 0.0323, 0.3233);
            AlgorithmStub s = new AlgorithmStub(new List<Activity>(), m);

            Plan plan = s.PlanTemplate();

            Activity x = new Activity(new Event("Default", new Value(20, 10, 0, 1), new Time_Scheduler.Models.Temporal.Domain(
                new DateTime(2016, 04, 02, 12, 30, 00),
                new DateTime(2016, 04, 02, 13, 30, 00)), m.InitialUser));

            if(!s.Add(plan, x.Reference))
            {
                Assert.Fail();
            }

            if(plan.Count != 3)
            {
                Assert.Fail();
            }

        }

        class AlgorithmStub : Algorithm
        {

            public override string Name
            {
                get { return ""; }
            }

            public AlgorithmStub(List<Activity> activities, Map map)
                : base(activities,map)
            {

            }

            public override Plan Best()
            {
                throw new NotImplementedException();
            }

            public override void Execute()
            {
                throw new NotImplementedException();
            }

            public override void Initialize()
            {
                throw new NotImplementedException();
            }
        }
    }
}
