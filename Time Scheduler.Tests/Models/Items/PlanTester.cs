﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Time_Scheduler.Models;
using Time_Scheduler.Models.Items;
using Time_Scheduler.Models.Travel;

namespace Time_Scheduler.Tests.Models.Items
{
    [TestClass]
    public class PlanTester
    {

        [TestMethod]
        public void AddBefore()
        {
            Plan p = new Plan();
            Activity x = new Activity();
            p.AddFirst(x);
            p.AddLast(new Activity());
            p.AddBefore(
                p.Last,
                new Activity(
                    new Event("asdf", new Value(5, 10, 0, 1), new Time_Scheduler.Models.Temporal.Domain(new DateTime(2015, 12, 12, 12, 30, 00), new DateTime(2015, 12, 12, 12, 30, 00)), new Location("adsf", 0.343,30)))
                {
                    Start =new DateTime(2015,12,12, 12, 30, 00),
                    Finish = new DateTime(2015, 12, 13, 12, 30, 00)
                });
            if(p.Value.Importance != 1440*5)
            {
                Assert.Fail();
            }

        }

        [TestMethod]
        public void AddAfter()
        {
            Plan p = new Plan();
            Activity x = new Activity();
            p.AddFirst(x);
            p.AddLast(new Activity());
            p.AddAfter(
                p.First,
                new Activity(
                    new Event("asdf", new Value(5, 10, 0, 1), new Time_Scheduler.Models.Temporal.Domain(new DateTime(2015, 12, 12, 12, 30, 00), new DateTime(2015, 12, 12, 12, 30, 00)), new Location("adsf", 0.343, 30)))
                {
                    Start = new DateTime(2015, 12, 12, 12, 30, 00),
                    Finish = new DateTime(2015, 12, 13, 12, 30, 00)
                });
            if (p.Value.Importance != 1440 * 5)
            {
                Assert.Fail();
            }
        }

    }
}
