﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Time_Scheduler.Models.Travel;

namespace Time_Scheduler.Tests.Models.Travel
{
    [TestClass]
    public class LocationTester
    {
        [TestMethod]
        public void LocationSuitability()
        {
            Location l = new Location("Test 1", 0.043, 0.4534);

            Assert.IsTrue(l.IsSuitableFor(Transport.Walk));
            Assert.IsFalse(l.IsSuitableFor(Transport.Car));

            l.AddSuitabilityFor(Transport.Car);
            Assert.IsTrue(l.IsSuitableFor(Transport.Car));

        }

        [TestMethod]
        public void DistanceCalculator()
        {
            Location l1 = new Location("Test 1", 53.946939, -1.055307);
            Location l2 = new Location("Test 2", 53.948701, -1.051735);

            double x = l1.DistanceFrom(l2);


            //some reasonable accuraccy 
            if (x < 300 && x > 310)
            {
                Assert.Fail();
            }
        }
    }
}
