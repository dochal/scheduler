﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Time_Scheduler.Models.Travel;

namespace Time_Scheduler.Tests.Models.Travel
{
    [TestClass]
    public class MapTester
    {
        [TestMethod]
        public void Shortest_Walking()
        {
            Location l1 = new Location("Home", 53.946939, -1.055307);
            Location l2 = new Location("Bus Stop", 53.948701, -1.051735);

            Map m = new Map();
            Journey j = m.Shortest(l1, l2);

            if (j.Transport != Transport.Walk)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void Shortest_Cycle()
        {
            Location l1 = new Location("Home", 53.946939, -1.055307);
            Location l2 = new Location("CS Department", 53.946818, -1.030764);

            Map m = new Map();
            Journey j = m.Shortest(l1, l2);

            if (j.Transport != Transport.Bike)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void Shortest_Transit()
        {
            Location l1 = new Location("Home", 53.946939, -1.055307);
            Location l2 = new Location("Train Station", 53.957685, -1.093872);

            Map m = new Map();
            Journey j = m.Shortest(l1, l2);

            if (j.Transport != Transport.PublicTransport)
            {
                Assert.Fail();
            }
        }

        [TestMethod]
        public void Shortest_Car()
        {
            Location l1 = new Location("Home", 53.946939, -1.055307);
            Location l2 = new Location("London", 51.498844, -0.201400);

            Map m = new Map();
            Journey j = m.Shortest(l1, l2);

            if (j.Transport != Transport.Car)
            {
                Assert.Fail();
            }
        }
    }
}
