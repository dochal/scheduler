﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Time_Scheduler.Models.Travel;

namespace Time_Manager
{
    public class UoY
    {
        public Location Shop = new Location("Shop", 53.948397, -1.053488);
        public Location Home = new Location("Home", 53.946176, -1.054107);
        public Location West = new Location("Heslington West", 53.948149, -1.053304);
        public Location East = new Location("Heslington East", 53.948018, -1.030089);
        public Location Library = new Location("Library", 53.949273, -1.052006);
        public Location Gym = new Location("Gym", 53.943690, -1.056031);
        public Location Centre = new Location("York City Center", 53.957926, -1.080026);
        public Location BusStation = new Location("Bus Station", 53.958339, -1.080289);
        public Location TrainStation = new Location("Train Station", 53.957991, -1.092840);

    }
}
