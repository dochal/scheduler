﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Time_Scheduler.Components;
using Time_Scheduler.Models.Items;
using Time_Scheduler.Models.Temporal;
using Time_Scheduler.Models.Travel;
using Time_Scheduler.Models;
namespace Time_Manager.Samples
{
    public class Academic : List<Schedulable>, ISample
    {

        public Academic(UoY map)
        {
            
            for (int i = 0; i < 363; i++)
            {
                //once a week
                if (i % 7 == 0)
                {
                    this.Add(new Task("Rubbish Out", new Value(10, 0, 0, -1), false,
                        new Domain(new DateTime(2015, 9, 30, 18, 00, 00).AddDays(i), new DateTime(2015, 9, 30, 22, 00, 00).AddDays(i)),
                        map.Home,
                        new Range(TimeSpan.FromMinutes(1), TimeSpan.FromMinutes(1)),
                        TimeSpan.FromHours(2)));


                    List<Domain> occurences = new List<Domain>();
                    occurences.Add(new Domain(new DateTime(2015, 10, 1, 12, 30, 00).AddDays(i), new DateTime(2015, 10, 1, 22, 00, 00).AddDays(i)));
                    occurences.Add(new Domain(new DateTime(2015, 10, 2, 12, 30, 00).AddDays(i), new DateTime(2015, 10, 2, 22, 00, 00).AddDays(i)));
                    occurences.Add(new Domain(new DateTime(2015, 10, 3, 12, 30, 00).AddDays(i), new DateTime(2015, 10, 3, 22, 00, 00).AddDays(i)));

                    this.Add(new Task("Shopping", new Value(10,0,-40,2), false,
                        occurences,
                        map.Shop,
                        new Range(TimeSpan.FromHours(1), TimeSpan.FromHours(2)),
                        TimeSpan.FromHours(2)));

                    this.Add(new Task("Laundry", new Value(10, 0, -3, -1), false,
                        occurences,
                        map.Home,
                        new Range(TimeSpan.FromHours(1), TimeSpan.FromHours(2)),
                        TimeSpan.FromHours(2)
                        ));

                    Goal opensource = new Goal("Open Source Contribution", new Value(2, 0, 0, 10),
                        new Domain(new DateTime(2015, 10, 1, 7, 30, 00).AddDays(i), new DateTime(2016, 10, 7, 22, 00, 00).AddDays(i)),
                        map.Home, new Range(TimeSpan.FromHours(2), TimeSpan.FromHours(4)), TimeSpan.FromHours(20));
                    opensource.Locations.Add(map.East);
                    opensource.Locations.Add(map.Library);

                    this.Add(opensource);

                    Goal gaming = new Goal("Gaming", new Value(0, -1, 0, 20),
                        new Domain(new DateTime(2015, 10, 1, 7, 30, 00).AddDays(i), new DateTime(2016, 10, 7, 22, 00, 00).AddDays(i)),
                        map.Home, new Range(TimeSpan.FromHours(2), TimeSpan.FromHours(4)), TimeSpan.FromHours(20));

                    this.Add(gaming);

                }

                if (i % 3 == 0)
                {

                    List<Domain> occurences = new List<Domain>();
                    occurences.Add(new Domain(new DateTime(2015, 10, 1, 16, 30, 00).AddDays(i), new DateTime(2015, 10, 1, 22, 00, 00).AddDays(i)));
                    occurences.Add(new Domain(new DateTime(2015, 10, 2, 16, 30, 00).AddDays(i), new DateTime(2015, 10, 2, 22, 00, 00).AddDays(i)));
                    occurences.Add(new Domain(new DateTime(2015, 10, 3, 16, 30, 00).AddDays(i), new DateTime(2015, 10, 3, 22, 00, 00).AddDays(i)));


                    this.Add(new Task("Gym", new Value(3, 20, -2, 4), false, occurences, map.Gym, new Range(
                            TimeSpan.FromMinutes(45),
                            TimeSpan.FromHours(2)), TimeSpan.FromHours(2)));

                }

                //everyday
                this.Add(new Task("Sleep", new Value(20, 10, 0, 1), false,
                    new Domain(new DateTime(2015, 9, 30, 22, 30, 00).AddDays(i), new DateTime(2015, 10, 1, 8, 30, 00).AddDays(i)),
                    map.Home,
                    new Range(TimeSpan.FromHours(5), TimeSpan.FromHours(8)), TimeSpan.FromHours(8)));

                this.Add(new Task("Bathroom", new Value(3, 5, 0, 4), false,
                    new Domain(new DateTime(2015, 10, 1, 6, 30, 00).AddDays(i), new DateTime(2015, 10, 1, 9, 30, 00).AddDays(i)),
                    map.Home,
                    new Range(TimeSpan.FromMinutes(5), TimeSpan.FromMinutes(30)), TimeSpan.FromMinutes(30)));

                this.Add(new Task("Breakfast", new Value(3, 10, -2, 4), false,
                    new Domain(new DateTime(2015, 9, 30, 6, 30, 00).AddDays(i), new DateTime(2015, 10, 1, 10, 30, 00).AddDays(i)),
                    map.Home,
                    new Range(TimeSpan.FromMinutes(30), TimeSpan.FromMinutes(45)), TimeSpan.FromMinutes(45)));

                this.Add(new Task("Lunch", new Value(3, 10, -2, 4), false,
                    new Domain(new DateTime(2015, 10, 1, 11, 30, 00).AddDays(i), new DateTime(2015, 10, 1, 14, 00, 00).AddDays(i)),
                    new HashSet<Location> { map.Home, map.Library, map.East },
                    new Range(TimeSpan.FromMinutes(30), TimeSpan.FromHours(1)), TimeSpan.FromHours(1)));

                this.Add(new Task("Dinner", new Value(3, 10, -2, 6), false,
                    new Domain(new DateTime(2015, 10, 1, 16, 30, 00).AddDays(i), new DateTime(2015, 10, 1, 19, 30, 00).AddDays(i)),
                    map.Home,
                    new Range(TimeSpan.FromMinutes(30), TimeSpan.FromHours(1)), TimeSpan.FromHours(1)));

                this.Add(new Task("Supper", new Value(3, 10, -2, 6), false,
                    new Domain(new DateTime(2015, 10, 1, 20, 30, 00).AddDays(i), new DateTime(2015, 10, 1, 22, 30, 00).AddDays(i)),
                    map.Home,
                    new Range(TimeSpan.FromMinutes(15), TimeSpan.FromMinutes(45)),
                    TimeSpan.FromMinutes(45)));

                this.Add(new Task("Evening Bathroom", new Value(3, 7, 0, 4), false,
                    new Domain(new DateTime(2015, 10, 1, 22, 00, 00).AddDays(i), new DateTime(2015, 10, 1, 23, 30, 00).AddDays(i)),
                    map.Home,
                    new Range(TimeSpan.FromMinutes(15), TimeSpan.FromMinutes(30)),
                    TimeSpan.FromMinutes(45)));

            }
            Domain defaul2 = new Domain(new DateTime(2015, 10, 1), new DateTime(2016, 05, 10, 15, 00, 00));
            Range attention = new Range(TimeSpan.FromMinutes(45), TimeSpan.FromHours(3));
            Goal arts = new Goal("ARTS", new Value(20, 0, 0, 10), defaul2, map.Home, attention, TimeSpan.FromHours(200));
            arts.Locations.Add(map.East);
            arts.Locations.Add(map.Library);

            Goal iapt = new Goal("IAPT", new Value(20, 0, 0, 10), defaul2, map.Home, attention, TimeSpan.FromHours(200));
            iapt.Locations.Add(map.East);
            iapt.Locations.Add(map.Library);

            Goal icot = new Goal("ICOT", new Value(20, 0, 0, 10), defaul2, map.Home, attention, TimeSpan.FromHours(200));
            icot.Locations.Add(map.East);
            icot.Locations.Add(map.Library);

            Goal dams = new Goal("DAMS", new Value(20, 0, 0, 10), defaul2, map.Home, attention, TimeSpan.FromHours(200));
            dams.Locations.Add(map.East);
            dams.Locations.Add(map.Library);

            Goal project = new Goal("Project", new Value(20, 0, 0, 10), defaul2, map.Home, attention, TimeSpan.FromHours(400));
            project.Locations.Add(map.East);
            project.Locations.Add(map.Library);

            var reader = new StreamReader(File.OpenRead(@"Data\Timetable.csv"));
            while (!reader.EndOfStream)
            {
                string line = reader.ReadLine();
                string[] values = line.Split(',');

                if (values[6] == "Project")
                {
                    DateTime start = Convert.ToDateTime(values[0] + " " + values[1]);
                    DateTime end = Convert.ToDateTime(values[2] + " " + values[3]);
                    Event x = new Event("Project " + values[4], new Value(40, 0, 0, 10), new Domain(start, end), map.East);
                    this.Add(x);
                    project.Add(x);
                }
                else if (values[6] == "ARTS")
                {
                    DateTime start = Convert.ToDateTime(values[0] + " " + values[1]);
                    DateTime end = Convert.ToDateTime(values[2] + " " + values[3]);
                    Event x = new Event("ARTS " + values[4], new Value(40, 0, 0, 10), new Domain(start, end), map.East);
                    this.Add(x);
                    arts.Add(x);
                }
                else if (values[6] == "ICOT")
                {
                    DateTime start = Convert.ToDateTime(values[0] + " " + values[1]);
                    DateTime end = Convert.ToDateTime(values[2] + " " + values[3]);
                    Event x = new Event("ICOT " + values[4], new Value(40, 0, 0, 10), new Domain(start, end), map.East);
                    this.Add(x);
                    icot.Add(x);
                }
                else if (values[6] == "DAMS")
                {
                    DateTime start = Convert.ToDateTime(values[0] + " " + values[1]);
                    DateTime end = Convert.ToDateTime(values[2] + " " + values[3]);
                    Event x = new Event("DAMS " + values[4], new Value(40, 0, 0, 10), new Domain(start, end), map.East);
                    this.Add(x);
                    dams.Add(x);
                }
                else if (values[6] == "IAPT")
                {
                    DateTime start = Convert.ToDateTime(values[0] + " " + values[1]);
                    DateTime end = Convert.ToDateTime(values[2] + " " + values[3]);
                    Event x = new Event("IAPT " + values[4], new Value(40, 0, 0, 10), new Domain(start, end), map.East);
                    this.Add(x);
                    iapt.Add(x);
                }
                else
                {
                    DateTime start = Convert.ToDateTime(values[0] + " " + values[1]);
                    DateTime end = Convert.ToDateTime(values[2] + " " + values[3]);
                    Event x = new Event(values[4], new Value(40, 0, 0, 10), new Domain(start, end), map.East);
                    this.Add(x);
                }
            }

            this.Add(arts);
            this.Add(icot);
            this.Add(iapt);
            this.Add(dams);
            this.Add(project);

            this.Add(new Event("Christmas Dinner", new Value(80, 1, 0, 80), new Domain(new DateTime(2015, 12, 24, 15, 30, 00), new DateTime(2015, 12, 24, 20, 30, 00)), map.Home));
            this.Add(new Event("Christmas Day", new Value(40, 0, 0, 80), new Domain(new DateTime(2015, 12, 25, 10, 30, 00), new DateTime(2015, 12, 25, 20, 30, 00)), map.Home));
            this.Add(new Event("Christmas Day", new Value(40, 0, 0, 80), new Domain(new DateTime(2015, 12, 26, 10, 30, 00), new DateTime(2015, 12, 26, 20, 30, 00)), map.Home));
            for (int i = 0; i < 10; i++)
            {
                this.Add(new Event("Ballroom Beginners Standard", new Value(20, 20, -2, 10),
                        new Domain(
                            new DateTime(2015, 09, 30, 18, 30, 00).AddDays(i * 7 + 6),
                            new DateTime(2015, 09, 30, 19, 15, 00).AddDays(i * 7 + 6)),
                        map.West));

                this.Add(new Event("Ballroom Beginners Latin", new Value(20, 20, -2, 10), new Domain(new DateTime(2015, 09, 30, 19, 15, 00).AddDays(i * 7 + 6), new DateTime(2015, 09, 30, 20, 00, 00).AddDays(i * 7 + 6)), map.West));

                this.Add(new Event("Ballroom Advanced Standard", new Value(40, 30, -2, 30), new Domain(new DateTime(2015, 09, 30, 20, 30, 00).AddDays(i * 7 + 6), new DateTime(2015, 09, 30, 20, 45, 00).AddDays(i * 7 + 6)), map.West));

                this.Add(new Event("Ballroom Advanced Latin", new Value(40, 30, -2, 30), new Domain(new DateTime(2015, 09, 30, 20, 45, 00).AddDays(i * 7 + 6), new DateTime(2015, 09, 30, 21, 30, 00).AddDays(i * 7 + 6)), map.West));

            }

            for (int i = 0; i < 10; i++)
            {
                this.Add(
                    new Event("Ballroom Beginners Standard", new Value(40, 30, -2, 30),
                        new Domain(
                            new DateTime(2016, 01, 5, 18, 30, 00).AddDays(i * 7 + 6),
                            new DateTime(2016, 01, 5, 19, 15, 00).AddDays(i * 7 + 6)),
                        map.West));

                this.Add(new Event("Ballroom Beginners Latin", new Value(40, 30, -5, 30), new Domain(new DateTime(2016, 1, 5, 19, 15, 00).AddDays(i * 7 + 6), new DateTime(2016, 1, 5, 19, 20, 00, 00).AddDays(i * 7 + 6)), map.West));

                this.Add(new Event("Ballroom Advanced Standard", new Value(40, 30, -5, 30), new Domain(new DateTime(2016, 1, 5, 19, 20, 30, 00).AddDays(i * 7 + 6), new DateTime(2016, 1, 5, 19, 20, 45, 00).AddDays(i * 7 + 6)), map.West));

                this.Add(new Event("Ballroom Advanced Latin", new Value(40, 30, -5, 30), new Domain(new DateTime(2016, 1, 5, 19, 20, 45, 00).AddDays(i * 7 + 6), new DateTime(2016, 1, 5, 19, 21, 30, 00).AddDays(i * 7 + 6)), map.West));

            }

            DDay.iCal.IICalendarCollection calendars = DDay.iCal.iCalendar.LoadFromFile("Data/Barcelona.ics");
            foreach (var x in calendars)
            {
                foreach (DDay.iCal.Event q in x.Events)
                {
                    if (!q.IsAllDay && q.End.Local > new DateTime(2015, 09, 29, 18, 00, 00))
                        this.Add(new Event(q.Description + " " + q.Summary, new Value(40, 30, -5, 30), new Domain(q.Start.Local, q.End.Local), map.Home));
                }
            }
        }

        public string Name
        {
            get
            {
                return "Academic";
            }
        }
    }
}
