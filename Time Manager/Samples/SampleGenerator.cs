﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Time_Scheduler.Components;
using Time_Scheduler.Models;
using Time_Scheduler.Models.Items;
using Time_Scheduler.Models.Temporal;
using Time_Scheduler.Models.Travel;

namespace Time_Manager.Samples
{
    public class SampleGenerator
    {

        static Random r = new Random();
        public static Sample Generate(DateTime start, DateTime end, Map map, int events, int tasks, int goals)
        {
            Sample items = new Sample();
            for (int i = 0; i <= events; i++)
            {
                items.Add(NewEvent(start,end, map));
            }
            for (int i = 0; i <= tasks; i++)
            {
                items.Add(NewTask(start,end,map));
            }
            for (int i = 0; i <= goals; i++)
            {
                items.Add(NewGoal(start,end,map));
            }
            return items;
        }

        public static Goal NewGoal(DateTime start, DateTime end, Map map)
        {
            int min = r.Next(1, 6);
            int max = r.Next(min, 10);
            int length = r.Next(max, 50);
            int total = (int)(end - start).TotalMinutes;
            DateTime startDate = start.AddMinutes(r.Next(length * 30, total) - length * 30);
            return new Goal(
                "Goal " + r.Next(1, 432423423),
                new Value(40 - r.Next(0, 40), 20 - r.Next(0, 20), 200 - r.Next(0, 400), 100 - r.Next(0, 200)),
                new Domain(startDate, end),
                map.Locations.ElementAt(r.Next(0, map.Locations.Count)),
                new Range(TimeSpan.FromMinutes(min * 30), TimeSpan.FromMinutes(max * 30)),
                TimeSpan.FromMinutes(length * 30));
        }

        private static Event NewEvent(DateTime start, DateTime end, Map map)
        {
            int length = r.Next(1, 17);
            int total = (int)(end - start).TotalMinutes;
            DateTime startDate = start.AddMinutes(r.Next(length * 30, total) - length * 30);
            return new Event("Event " + r.Next(1, 432423423),
                new Time_Scheduler.Models.Value(
                    40 - r.Next(0, 40),
                    20 - r.Next(0, 20),
                    200 - r.Next(0, 400),
                    100 - r.Next(0, 200)
                ),
                new Time_Scheduler.Models.Temporal.Domain(startDate, startDate.AddMinutes(length * 30)),
                map.Locations.ElementAt(r.Next(0, map.Locations.Count)));
        }

        private static Task NewTask(DateTime start, DateTime end, Map map)
        {
            int min = r.Next(1, 6);
            int max = r.Next(min, 10);
            int length = r.Next(max, 50);
            int total = (int)(end - start).TotalMinutes;
            DateTime startDate = start.AddMinutes(r.Next(length * 30, total) - length * 30);
            return new Task(
                "Task " + r.Next(1, 432423423),
                new Value(40 - r.Next(0, 40), 20 - r.Next(0, 20), 200 - r.Next(0, 400), 100 - r.Next(0, 200)),
                length > max ? true : false,
                new Domain(startDate, end),
                map.Locations.ElementAt(r.Next(0, map.Locations.Count)),
                new Range(TimeSpan.FromMinutes(min * 30), TimeSpan.FromMinutes(max * 30)),
                TimeSpan.FromMinutes(length * 30));
        }
    }
}
