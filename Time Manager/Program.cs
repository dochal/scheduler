﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using Time_Manager.Samples;
using Time_Scheduler;
using Time_Scheduler.Components;
using Time_Scheduler.Models.Items;
using Time_Scheduler.Models.Temporal;
using Time_Scheduler.Models.Travel;

namespace Time_Manager
{
    class Program
    {
        static void Main(string[] args)
        {
            new Program();
        }
        
        public List<Schedulable> todos = new List<Schedulable>();

        private UoY york = new UoY();
        Map map;
        public Program()
        {
            Console.WriteLine("Welcome To Personal Time Manager");
            map = loadMap();
            new Scheduler(new Academic(york), map);
        }

        private Map loadMap()
        {
            List<Location> locations = new List<Location>();
            locations.AddRange(new List<Location>() { york.Shop, york.Home, york.West, york.East, york.Library, york.Gym, york.Centre, york.BusStation, york.TrainStation });

            Map m = new Map();
            m.InitialUser = york.Home;
            m.Locations = locations;

            return m;
        }
        private void tester()
        {
            DateTime start = Convert.ToDateTime("2015/10/01 00:00:00");
            DateTime end = Convert.ToDateTime("2016/06/01 00:00:00");
            for (int i = 100; i < 4000; i+=100)
            {
                for (int j = 0; j < 3; j++)
                {
                    int goals = i / 100;
                    Console.WriteLine("Execution:{0}, Samples:{1}", j, i);
                    new Scheduler(SampleGenerator.Generate(start, end, map, i, i, goals), map);
                }
            }
        }

    }
}
