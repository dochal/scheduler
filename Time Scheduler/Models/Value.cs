﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Time_Scheduler.Models
{
    public struct Value
    {

        public int Importance { get; set; }

        public int Health { get; set; }

        public int Finance { get; set; }

        public int Enjoyment { get; set; }


        public Value(int importance, int health, int finance, int enjoyment) : this()
        {
            this.Importance = importance;
            this.Health = health;
            this.Finance = finance;
            this.Enjoyment = enjoyment;
        }

        public void AddPositive(Value v)
        {
            this.Importance += v.Importance;
            this.Health += v.Health < 0 ? 0 : v.Health;
            this.Finance += v.Finance < 0 ? 0 : v.Finance;
            this.Enjoyment += v.Enjoyment < 0 ? 0 : v.Enjoyment;
        }

        public static Value operator +(Value x, Value y)
        {
            return new Value(x.Importance + y.Importance, x.Health + y.Health, x.Finance + y.Finance, x.Enjoyment + y.Enjoyment);
        }

        public static Value operator -(Value x, Value y)
        {
            return new Value(x.Importance - y.Importance, x.Health - y.Health, x.Finance - y.Finance, x.Enjoyment - y.Enjoyment);
        }

        public static Value operator *(int x, Value y)
        {
            return new Value(x * y.Importance, x * y.Health, y.Finance, x * y.Enjoyment);
        }

        public static Value ZERO
        {
            get
            {
                return new Value
                {
                    Importance = 0,
                    Health = 0,
                    Finance = 0,
                    Enjoyment = 0
                };
            }
        }

        public new string ToString()
        {
            return string.Format("{0},{1},{2},{3}",Importance,Health,Finance,Enjoyment);
        }
    }
}
