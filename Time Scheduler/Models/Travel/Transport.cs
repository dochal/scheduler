﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Time_Scheduler.Models.Travel
{
    public enum Transport
    {
        NA,
        Walk,
        Car,
        Bike,
        PublicTransport
    }
}
