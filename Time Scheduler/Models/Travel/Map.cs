﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Time_Scheduler.Models.Travel
{
    /// <summary>
    /// Creates a new map
    /// </summary>
    public class Map
    {
        /// <summary>
        /// All locations are available, but it may take a very long time to get
        /// </summary>
        public List<Location> Locations = new List<Location>();

        /// <summary>
        /// The initial location of the user
        /// </summary>
        public Location InitialUser { get; set; }

        /// <summary>
        /// Calculates the shortest distance between the two locations
        /// </summary>
        /// <param name="start">Starting location</param>
        /// <param name="end">Ending location</param>
        /// <returns>The approproate journey between A and B</returns>
        public Journey Shortest(Location start, Location end)
        {
            double distance = end.DistanceFrom(start);
            Journey journey = new Journey();
            journey.Start = start;
            journey.End = end;

            if(distance < 0.1)
            {
                journey.Transport = Transport.NA;
                journey.Time = TimeSpan.Zero;
            }
            else if (walk(distance) < TimeSpan.FromMinutes(7))
            {
                journey.Time = walk(distance);
                journey.Transport = Transport.Walk;
            }
            else if (cycle(distance) < TimeSpan.FromMinutes(15) && Settings.Bike)
            {
                journey.Transport = Transport.Bike;
                journey.Time = cycle(distance);
            }
            else if (transit(distance) < TimeSpan.FromMinutes(30) && Settings.Transit)
            {
                journey.Transport = Transport.PublicTransport;
                journey.Time = transit(distance);
            }
            else
            {
                journey.Transport = Transport.Car;
                journey.Time =drive(distance);
            }

            return journey;
        }
        /// <summary>
        /// Calculates the temporal distance between two locations when walking
        /// </summary>
        /// <param name="distance"></param>
        /// <returns></returns>
        private TimeSpan walk(double distance)
        {
            return TimeSpan.FromSeconds(distance / 1.3);
        }
        /// <summary>
        /// Calculates the temporal distance between two locations when cycling
        /// </summary>
        /// <param name="distance"></param>
        /// <returns></returns>
        private TimeSpan cycle(double distance)
        {
            return TimeSpan.FromSeconds(120 + distance / 3.2);
        }

        /// <summary>
        /// Calculates the temporal distance between two locations when driving
        /// </summary>
        /// <param name="distance"></param>
        /// <returns></returns>
        private TimeSpan drive(double distance)
        {
            return TimeSpan.FromSeconds(600 + distance / 13.5);
        }

        /// <summary>
        /// Calculates the temporal distance between two locations when public transport
        /// </summary>
        /// <param name="distance"></param>
        /// <returns></returns>
        private TimeSpan transit(double distance)
        {
            return TimeSpan.FromSeconds(480 + distance / 8.5);
        }

    }
}
