﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Time_Scheduler.Models.Travel
{
    /// <summary>
    /// Contains times, locations amd
    /// </summary>
    public class Journey
    {
        /// <summary>
        /// Contains the starting location
        /// </summary>
        public Location Start { get; set; }

        /// <summary>
        /// Contains the end location
        /// </summary>
        public Location End { get; set; }

        /// <summary>
        /// Contains the method of transport between Start and End
        /// </summary>
        public Transport Transport { get; set; }

        /// <summary>
        /// Contains the time required to move from Start to End
        /// </summary>
        public TimeSpan Time { get; set; }


    }
}
