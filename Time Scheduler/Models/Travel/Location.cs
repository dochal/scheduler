﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Time_Scheduler.Models.Travel
{
    public class Location
    {
        /// <summary>
        /// Contains the name of the location as seen by the user
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Location's latitude
        /// </summary>
        public double Latitude { get; set; }

        /// <summary>
        /// Location's longitude
        /// </summary>
        public double Longitude { get; set; }

        HashSet<Transport> suitable = new HashSet<Transport>();
        
        /// <summary>
        /// Creates a new location
        /// </summary>
        /// <param name="name">Name of the locaton as seen by the user</param>
        /// <param name="latitude">the latitude</param>
        /// <param name="longitude">the longitude</param>
        public Location(string name, double latitude, double longitude)
        {
            Name = name;
            Latitude = latitude;
            Longitude = longitude;

            //Walking is available at any location
            suitable.Add(Transport.Walk);
        }

        /// <summary>
        /// Checks if the locations is suitable to leave a transport
        /// </summary>
        /// <param name="transport"></param>
        /// <returns></returns>
        public bool IsSuitableFor(Transport transport)
        {
            return suitable.Contains(transport);
        }

        /// <summary>
        /// Adds an alternative transport suitability
        /// </summary>
        /// <param name="transport"></param>
        /// <returns></returns>
        public bool AddSuitabilityFor(Transport transport)
        {
            if(suitable.Contains(transport))
            {
                return false;
            }
            else
            {
                suitable.Add(transport);
                return true;
            }
        }

        public double DistanceFrom(Location x)
        {
            //Convert to radians
            double sLatitudeRadians     = x.Latitude * (Math.PI / 180.0);
            double sLongitudeRadians    = x.Longitude * (Math.PI / 180.0);
            double eLatitudeRadians     = Latitude * (Math.PI / 180.0);
            double eLongitudeRadians    = Longitude * (Math.PI / 180.0);

            //calcualate different
            double dLongitude   = eLongitudeRadians - sLongitudeRadians;
            double dLatitude    = eLatitudeRadians - sLatitudeRadians;


            double result1 = Math.Pow(Math.Sin(dLatitude / 2.0), 2.0) +
                          Math.Cos(sLatitudeRadians) * Math.Cos(eLatitudeRadians) *
                          Math.Pow(Math.Sin(dLongitude / 2.0), 2.0);

            double result2 = 6371000 * 2.0 *
                          Math.Atan2(Math.Sqrt(result1), Math.Sqrt(1.0 - result1));

            return result2;
        }

    }
}
