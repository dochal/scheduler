﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Time_Scheduler.Models.Items;

namespace Time_Scheduler.Models
{
    public class Sample : List<Schedulable>
    {

        public Value Value { get; set; }

        public new void Add(Schedulable a)
        {
            this.Value.AddPositive((int)a.TotalTime.TotalMinutes * a.Value);
            base.Add(a);
        }

    }
}
