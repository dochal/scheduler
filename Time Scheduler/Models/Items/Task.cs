﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Time_Scheduler.Components;
using Time_Scheduler.Models.Temporal;
using Time_Scheduler.Models.Travel;

namespace Time_Scheduler.Models.Items
{
    /// <summary>
    /// An interruptable input item
    /// </summary>
    public class Task : Schedulable
    {
        /// <summary>
        /// Sets possible temporal domains
        /// </summary>
        private List<Domain> temporalDomain = new List<Domain>();

        public override List<Domain> TemporalDomain {
            get
            {
                return temporalDomain;
            }
        }

        /// <summary>
        /// Hashset of locations available when the task can be performed
        /// </summary>
        private HashSet<Location> locations = new HashSet<Location>();
        public override HashSet<Location> Locations
        {
            get
            {
                return locations;
            }
        }

        /// <summary>
        /// The range of possible durations for the task
        /// </summary>
        public override Range Durations { get; }

        /// <summary>
        /// Sets the total time required to complete the time
        /// </summary>
        public override TimeSpan TotalTime { get; }
        
        public Task(string name, Value value, bool interruptible, Domain defaultDomain, Location defaultLocation, Range minMaxDuration, TimeSpan total)
            : base(name, value, interruptible)
        {
            this.temporalDomain.Add(defaultDomain);
            this.locations.Add(defaultLocation);
            this.Durations = minMaxDuration;
            this.TotalTime = total;
        }
        public Task(string name, Value value, bool interruptible, Domain defaultDomain, HashSet<Location> defaultLocation, Range minMaxDuration, TimeSpan total)
            : base(name, value, interruptible)
        {
            this.temporalDomain.Add(defaultDomain);
            this.locations = defaultLocation;
            this.Durations = minMaxDuration;
            this.TotalTime = total;
        }

        public Task(string name, Value value, bool interruptible, List<Domain> defaultDomain, Location defaultLocation, Range minMaxDuration, TimeSpan total)
            : base(name, value, interruptible)
        {
            this.temporalDomain = defaultDomain;
            this.locations.Add(defaultLocation);
            this.Durations = minMaxDuration;
            this.TotalTime = total;
        }

        public void AddLocation(Location location)
        {
            this.locations.Add(location);
        }
        
        public void AddDomain(Domain domain)
        {
            this.temporalDomain.Add(domain);
        }

    }
}
