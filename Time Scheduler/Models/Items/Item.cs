﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Time_Scheduler.Models.Items
{
    public abstract class Item
    {

        private readonly int id;

        public int Id
        {
            get
            {
                return id;
            }
        }

        public readonly Value Value;

        public readonly string Name;

        public Item(string name, Value value)
        {
            this.id = Settings.Random.Next(1, Settings.MAX);
            this.Value = value;
            this.Name = name;
        }

    }
}
