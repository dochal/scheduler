﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Time_Scheduler.Components;
using Time_Scheduler.Models.Temporal;
using Time_Scheduler.Models.Travel;

namespace Time_Scheduler.Models.Items
{
    /// <summary>
    /// A single event with a list of alternatives
    /// </summary>
    public class Event : Schedulable
    {

        /// <summary>
        /// List of occurrences
        /// </summary>
        private List<Domain> occurrences = new List<Domain>();

        public override List<Domain> TemporalDomain
        {
            get
            {
                return occurrences;
            }
        }
        
        /// <summary>
        /// Sets the location of the event
        /// </summary>
        public Location Location { get; }

        public override TimeSpan TotalTime
        {
            get
            {
                return TemporalDomain.ElementAt(0).Difference;
            }
        }

        public override Range Durations
        {
            get
            {
                return new Range(TemporalDomain.ElementAt(0).Difference, TemporalDomain.ElementAt(0).Difference);
            }
        }

        private HashSet<Location> locations = new HashSet<Location>();
        public override HashSet<Location> Locations
        {
            get
            {
                return locations;
            }
        }

        /// <summary>
        /// Creates a new event with default properties.
        /// </summary>
        /// <param name="name">The name of the event as seen in the calendar</param>
        /// <param name="importance">Absolute importance of the task</param>
        /// <param name="occurrence">Default occurrence</param>
        public Event(string name, Value value, Domain defaultOccurrence, Location location)
            : base(name, value, false)
        {
            this.locations.Add(location);
            this.TemporalDomain.Add(defaultOccurrence);
        }
    }
    
}
