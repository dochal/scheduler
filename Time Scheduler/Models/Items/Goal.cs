﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Time_Scheduler.Components;
using Time_Scheduler.Models.Temporal;
using Time_Scheduler.Models.Travel;

namespace Time_Scheduler.Models.Items
{
    public class Goal : Task
    {
         
        public Goal(string name, Value value, Domain defaultDomain, Location defaultLocation, Range durations, TimeSpan total)
            : base(name, value, true, defaultDomain, defaultLocation, durations, total)
        {

        }

        public void Add(Schedulable activity)
        {
            activity.setParent(this);
        }
        
    }
}
