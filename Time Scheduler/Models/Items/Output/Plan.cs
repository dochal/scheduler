﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Time_Scheduler.Models.Items
{
    public class Plan : LinkedList<Activity>
    {
        
        Dictionary<int, Pair<TimeSpan, Schedulable>> times = new Dictionary<int, Pair<TimeSpan, Schedulable>>();
        
        public new void AddAfter(LinkedListNode<Activity> node, Activity activity)
        {
            if (!times.ContainsKey(activity.Reference.Id) || (times[activity.Reference.Id].Item1 <= activity.Reference.TotalTime && activity.Reference.Interruptible))
            {
                addTime(activity.Duration, activity.Reference);
                base.AddAfter(node, activity);
            }
        }

        private void addTime(TimeSpan duration, Schedulable item)
        {
            if(times.ContainsKey(item.Id))
            {
                times[item.Id].Item1 = times[item.Id].Item1 + duration;
            }
            else
            {
                times.Add(item.Id, new Pair<TimeSpan,Schedulable>(duration, item));
            }
            if(item.Parent != null)
            {
                addTime(duration, item.Parent);
            }
        }

        public new void AddBefore(LinkedListNode<Activity> node, Activity activity)
        {
            if (!times.ContainsKey(activity.Reference.Id) || (times[activity.Reference.Id].Item1 <= activity.Reference.TotalTime && activity.Reference.Interruptible))
            {
                addTime(activity.Duration, activity.Reference);
                base.AddAfter(node, activity);
            }

        }

        public Value Value
        {
            get
            {
                Value value = Value.ZERO;
                foreach (var t in times)
                {
                    // If exeeded that just return the maximum value
                    if (t.Value.Item1 > t.Value.Item2.TotalTime)
                    {
                        value += ((int)t.Value.Item2.TotalTime.TotalMinutes * t.Value.Item2.Value);
                    }
                    else
                    {
                        value += ((int)t.Value.Item1.TotalMinutes * t.Value.Item2.Value);
                    }
                }
                return value;
            }
        }

    }
    internal class Pair<T1, T2>
    {

        public Pair(T1 item1, T2 item2)
        {
            this.Item1 = item1;
            this.Item2 = item2;
        }

        public T1 Item1 { get; set; }
        public T2 Item2 { get; set; }
    }
}
