﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Time_Scheduler.Components;
using Time_Scheduler.Models.Temporal;
using Time_Scheduler.Models.Travel;

namespace Time_Scheduler.Models.Items
{
    public class Activity
    {
        /// <summary>
        /// The start of the activity (ex. travelling)
        /// </summary>
        public DateTime Start { get; set; }

        /// <summary>
        /// Sets the start of the activity
        /// </summary>
        public DateTime Finish { get; set; }

        /// <summary>
        /// get the beginning of the activity (inc. travelling)
        /// </summary>
        public DateTime Begin { get; set; }

        /// <summary>
        /// Gets the end of the activity 
        /// </summary>
        public DateTime End { get; set; }
        
        /// <summary>
        /// Gets the location where the activity occurs
        /// </summary>
        public Location Location { get; set; }
        
        /// <summary>
        /// Gets the method of transport
        /// </summary>
        public Transport Transport { get; set; }

        public volatile Schedulable Reference;

        public Activity(Schedulable x)
        {
            this.Reference = x;
        }
        public Activity()
        {

        }

        public TimeSpan Duration
        {
            get
            {
                return Finish - Start;
            }
        }

    }
}
