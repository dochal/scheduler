﻿using System;
using System.Collections.Generic;
using Time_Scheduler.Components;
using Time_Scheduler.Models.Temporal;
using Time_Scheduler.Models.Travel;

namespace Time_Scheduler.Models.Items
{
    /// <summary>
    /// Item which is included in scheduling 
    /// </summary>
    public abstract class Schedulable : Item
    {

        public Goal Parent { get; set; }
        
        public readonly bool Interruptible;

        public abstract List<Domain> TemporalDomain { get; }

        public abstract HashSet<Location> Locations { get; }
        
        public Schedulable(string name, Value value, bool interruptible)
            :base(name, value)
        {
            this.Interruptible = interruptible;
        }

        public TimeSpan AvailableTime
        {
            get
            {
                TimeSpan t = TimeSpan.Zero;
                foreach(Domain d in TemporalDomain)
                {
                    t += d.Difference;
                }
                return t;
            }
        }

        public void setParent(Goal goal)
        {
            this.Parent = goal;
        }
        
        public abstract TimeSpan TotalTime { get; }

        public abstract Range Durations { get; }
        
    }
}
