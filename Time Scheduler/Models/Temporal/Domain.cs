﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Time_Scheduler.Components;

namespace Time_Scheduler.Models.Temporal
{
    /// <summary>
    /// Denotes the possible occcurences of the task, without creating a list for every possible
    /// occurence in terms of time
    /// </summary>
    public class Domain : Interval
    {
        
        /// <summary>
        /// Defines a new domain for the task with startline and deadline
        /// </summary>
        /// <param name="startline"></param>
        /// <param name="deadline"></param>
        public Domain(DateTime startline, DateTime deadline)
            : base(startline, deadline)
        {

        }

    }
}
