﻿using JMetalCSharp.Core;
using JMetalCSharp.Encoding.SolutionType;
using JMetalCSharp.Encoding.Variable;
using JMetalCSharp.Operators.Crossover;
using JMetalCSharp.Operators.Mutation;
using JMetalCSharp.Operators.Selection;
using JMetalCSharp.QualityIndicator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Time_Scheduler.Models.Items;
using Time_Scheduler.Models.Travel;

namespace Time_Scheduler.Algorithms
{
    /// <summary>
    /// Uses a random approach to assigning values
    /// </summary>
    public class Genetic : Algorithm
    {
        JMetalCSharp.Core.Algorithm algorithm;
        Solution _top;

        public override string Name
        {
            get
            {
                return "Genetic Variable Assignment";
            }
        }

        public Genetic(List<Activity> activities, Map map)
            : base(activities,map)
        {

        }
        
        public override Plan Best()
        {
            throw new NotImplementedException();
        }

        public override void Execute()
        {

            // Execute the Algorithm
            SolutionSet population = algorithm.Execute();
            _top = population.SolutionsList.ElementAt(0);

        }

        public override void Initialize()
        {
            // Initializing Time Management Problem
            TimeManagementProblem x = new TimeManagementProblem(this);
            algorithm = new JMetalCSharp.Metaheuristics.NSGAII.NSGAII(x);

            // Algorithm parameters
            algorithm.SetInputParameter("populationSize", 500);
            Console.WriteLine("Population Size 500");
            algorithm.SetInputParameter("maxEvaluations", 10000);
            Console.WriteLine("Max Evaluations 10000");

            // Crossver operator
            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("probability", 0.90);
            Operator crossover = CrossoverFactory.GetCrossoverOperator("SinglePointCrossover", parameters);

            // Mutation operator
            parameters = new Dictionary<string, object>();
            parameters.Add("probability", 0.2);
            Operator mutation = MutationFactory.GetMutationOperator("BitFlipMutation", parameters);

            // Selection Operator
            Operator selection = SelectionFactory.GetSelectionOperator("BinaryTournament2", null);

            // Add the operators to the algorithm
            algorithm.AddOperator("crossover", crossover);
            algorithm.AddOperator("mutation", mutation);
            algorithm.AddOperator("selection", selection);
            algorithm.SetInputParameter("indicators", null);
        }

        internal class TimeManagementProblem : Problem
        {
            Algorithm algorithm;

            public TimeManagementProblem(Algorithm algorithm)
            {
                this.algorithm = algorithm;

                NumberOfObjectives = 2;
                //one for start, end and location
                NumberOfVariables = algorithm.Activities.Count * 3;
                SolutionType = new TimeManagementSolution(this, algorithm.Activities);
            }

            public override void Evaluate(Solution solution)
            {
                //Construct solution
                // Is a timeline 
                Activity[] overlap = new Activity[(int)(Settings.EndDate - Settings.StartDate).TotalMinutes];

                Dictionary<int, int> time = new Dictionary<int, int>();
                int overlaps = 0;
                int valuable = 0;
                // For each 
                for (int i = 0; i < NumberOfVariables; i += 3)
                {
                    int duration = ((Int)solution.Variable[i]).Value;
                    if (duration > 0)
                    {
                        int begin = ((Int)solution.Variable[i + 1]).Value;
                        int location = ((Int)solution.Variable[i + 2]).Value;
                        int index = i / 3;
                        Activity a = this.algorithm.Activities.ElementAt(index);

                        for (int x = begin; x < begin + duration; x++)
                        {
                            //We have something already scheduled at this time
                            if (overlap[x] != null)
                            {
                                overlaps++;
                            }
                            else
                            {
                                overlap[x] = a;
                                int total = (int)a.Reference.TotalTime.TotalMinutes;
                                if (time[a.Reference.Id] <= total)
                                {
                                    time[a.Reference.Id]++;
                                    Goal p = a.Reference.Parent;
                                    while (p != null)
                                    {
                                        time[a.Reference.Id]++;
                                        p = p.Parent;
                                    }
                                }
                            }
                        }
                    }
                }
                solution.Objective[0] = overlaps - overlap.Length;
                solution.Objective[1] = valuable;

            }
        }

        /// <summary>
        /// Time management is modelled as an integer solution. 
        /// </summary>
        internal class TimeManagementSolution : IntSolutionType
        {
            /// <summary>
            /// Contains the activities which can be scheduled
            /// </summary>
            List<Activity> activities;

            public TimeManagementSolution(Problem problem, List<Activity> activities) : base(problem)
            {
                this.activities = activities;
            }




            /// <summary>
            /// Creates the variables of the solution. This is not completely random as we do have AN idea of how the schedule should look like
            /// </summary>
            /// <returns></returns>
            public override JMetalCSharp.Core.Variable[] CreateVariables()
            {
                Random r = new Random();
                // Index of the variable. Each variable contains: duration(i), beginning(i+1), location (i+2)
                int i = -1;


                Variable[] variables = new Variable[Problem.NumberOfVariables];
                foreach (Activity x in activities)
                {
                    // The likelihood of something being scheduled is dependent on the priority: the lower the priroity the smaller the likelihood.
                    variables[++i] = r.Next(0, x.Reference.Value.Importance) > 0
                        ? new Int((int)x.Reference.Durations.Minimum.TotalMinutes, (int)x.Reference.Durations.Maximum.TotalMinutes)
                        : new Int(0, (int)x.Reference.Durations.Minimum.TotalMinutes, (int)x.Reference.Durations.Maximum.TotalMinutes);

                    // The start of the activity can be any value between the beginning or deadline - value

                    DateTime min = x.Reference.TemporalDomain.ElementAt(0).Start;
                    DateTime max = x.Reference.TemporalDomain.ElementAt(1).End - TimeSpan.FromMinutes(((Int)variables[i]).Value);
                    variables[++i] = new Int(Settings.getInt(min), Settings.getInt(max));

                    // The location of the activity
                    variables[++i] = new Int(0, x.Reference.Locations.Count - 1);

                }
                return variables;
            }


        }
    }

    
}
