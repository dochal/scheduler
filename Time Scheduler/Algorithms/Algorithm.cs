﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Time_Scheduler.Models.Items;
using Time_Scheduler.Models.Temporal;
using Time_Scheduler.Models.Travel;

namespace Time_Scheduler.Algorithms
{
    public abstract class Algorithm
    {

        public volatile List<Activity> Activities;
        public volatile Map Map;

        public abstract string Name { get; }

        public Algorithm(List<Activity> activites, Map map)
        {
            Activities = activites;
            Map = map;
        }

        public abstract void Initialize();

        public abstract void Execute();
        
        public bool Add(Plan plan, Schedulable item)
        {
            var currentNode = plan.First;
            Activity current = null;
            Activity next = null;
            // Intervals must be ordered and non-overlapping for this to work.
            foreach (Domain d in item.TemporalDomain)
            {
                while (currentNode.Next != null)
                {
                    current = currentNode.Value;
                    next = currentNode.Next.Value;


                    if (d.Start > next.Begin)
                    {
                        currentNode = currentNode.Next;
                        continue;
                    }
                    // if the current interval ends before the beginning of this activity
                    // or it starts after the beginning of the next one, move to the next interval
                    if (current.Begin > d.End)
                    {
                        //if this is the case try the next interval
                        break;
                    }


                    DateTime earilest = this.earilestDate(current, d.Start);
                    DateTime latest = this.latestDate(next, d.End);
                    TimeSpan availableTime = latest - earilest;

                    // check if the time difference is big enough to accommodate the new activity
                    // it is not guaranteed as there is also travelling time to be accommodated
                    // this allows quickly getting to the next node rather than perform travelling calculations
                    if (availableTime < item.Durations.Minimum)
                    {
                        currentNode = currentNode.Next;
                        continue;
                    }

                    Tuple<Journey, Journey> distancePair = getJourneys(current, next, item);

                    DateTime begin = beginDate(current, earilest, distancePair.Item1.Time);
                    DateTime start = begin + distancePair.Item1.Time;
                    DateTime nextBegin = nextBeginDate(next, latest, distancePair.Item2.Time);

                    TimeSpan duration = nextBegin - start >= item.Durations.Maximum ? item.Durations.Maximum : nextBegin - start;

                    if (duration >= item.Durations.Minimum)
                    {
                        Activity activity = new Activity(item);
                        activity.Begin = begin;
                        activity.End = start;
                        activity.Start = start;
                        activity.Finish = start + duration;
                        activity.Location = distancePair.Item1.End;
                        activity.Transport = distancePair.Item1.Transport;

                        //We increamented the node before therefore currentNode has the next element
                        plan.AddAfter(currentNode, activity);

                        next.Begin = nextBegin;
                        next.Transport = distancePair.Item2.Transport;
                        return true;
                    }
                    currentNode = currentNode.Next;
                }
            }
            return false;
        }

        private DateTime earilestDate(Activity current, DateTime domainStart)
        {
            // if the current finishes later than the occurrence starts
            return current.Finish > domainStart ? current.Finish : domainStart;
        }

        private DateTime latestDate(Activity next, DateTime domainEnd)
        {
            // if the next starts sooner (not begins travel times will be calculated later)
            return next.Start < domainEnd ? next.Start : domainEnd;
        }

        private DateTime beginDate(Activity current, DateTime earliest, TimeSpan timeFromCurrentToNew)
        {
            // if you take away the journey time from the earliest point and that point occurs later than the finish of the current activity
            // then we can begin journey from there. Otherwise we will start the journey when the current activity finishes.
            return earliest - timeFromCurrentToNew >= current.Finish ? earliest - timeFromCurrentToNew : current.Finish;
        }

        private DateTime nextBeginDate(Activity next, DateTime latest, TimeSpan timeFromNewToNext)
        {
            // if we take away the journey time from the next
            // 
            return next.Start - timeFromNewToNext > latest ? next.Start - timeFromNewToNext : latest - timeFromNewToNext;
        }

        private Tuple<Journey, Journey> getJourneys(Activity current, Activity next, Schedulable item)
        {
            Journey travelFromCurrentToNew;
            Journey travelFromNewToNext;

            // If the current location is the same as the current activity
            if (item.Locations.Contains(current.Location))
            {
                // Should return 0
                travelFromCurrentToNew = Map.Shortest(current.Location, current.Location);

                // should return some value
                travelFromNewToNext = Map.Shortest(current.Location, next.Location);
            }
            else if (item.Locations.Contains(current.Location))
            {
                // Should return somevalue
                travelFromCurrentToNew = Map.Shortest(current.Location, next.Location);

                // should return 0
                travelFromNewToNext = Map.Shortest(current.Location, next.Location);
            }
            else
            {
                travelFromCurrentToNew = Map.Shortest(current.Location, item.Locations.First());
                travelFromNewToNext = Map.Shortest(item.Locations.First(), next.Location);
            }

            return new Tuple<Journey, Journey>(travelFromCurrentToNew, travelFromNewToNext);
        }

        public Plan PlanTemplate()
        {
            Plan plan = new Plan();
            plan.AddFirst(new Activity() { Transport = Transport.NA, Location = Map.InitialUser, Begin = Settings.StartDate, End = Settings.StartDate, Start = Settings.StartDate, Finish = Settings.StartDate });
            plan.AddLast(new Activity() { Transport = Transport.NA, Location = Map.InitialUser, Begin = Settings.EndDate, End = Settings.EndDate, Start = Settings.EndDate, Finish = Settings.EndDate });
            return plan;
        }

        public abstract Plan Best();

    }
}
