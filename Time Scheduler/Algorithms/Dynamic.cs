﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Time_Scheduler.Models.Items;
using Time_Scheduler.Models.Travel;

namespace Time_Scheduler.Algorithms
{
    public class Dynamic : Algorithm
    {
        public override string Name
        {
            get
            {
                return "Dynamic Algorithm";
            }
        }
        Plan p;

        public Dynamic(List<Activity> activities, Map map)
            : base(activities, map)
        {
        }

        public override Plan Best()
        {
            return p;
        }

        public override void Execute()
        {
            foreach (var i in this.Activities)
            {
                this.Add(p, i.Reference);
            }
        }

        public override void Initialize()
        {
            p = this.PlanTemplate();
            Activities = Activities.OrderBy(x => (x.Reference.AvailableTime) - x.Reference.TotalTime).ThenByDescending(x => x.Reference.Value.Importance).ToList();
        }
    }
}
