﻿using Microsoft.SolverFoundation.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Time_Scheduler.Models.Items;
using Time_Scheduler.Models.Travel;

namespace Time_Scheduler.Algorithms
{
    /// <summary>
    /// Uses mixed integer approach to solve the problem
    /// </summary>
    public class MixedInteger : Algorithm
    {

        private int length;
        Solution solution;

        public override string Name { get { return "Mixed Integer"; } }

        public MixedInteger(List<Activity> activities, Map map)
            : base(activities,map)
        {
        }

        public override Plan Best()
        {
            throw new NotImplementedException();
        }

        public override void Execute()
        {
            
            // Creating a solver context
            SolverContext context = SolverContext.GetContext();
            
            // Creating model for the this context to be solved
            Model model = context.CreateModel();


            // 
            Decision[] start = new Decision[length];
            Decision[] duration = new Decision[length];
            Decision[] location = new Decision[length];
            Decision[] schedulable = new Decision[length];

            string value = "";
            int i = 0;

            foreach (Activity a in Activities)
            {
                string name = Regex.Replace(a.Reference.Name + i, @"[^a-zA-Z0-9_]", "");

                start[i] = this.start(a, name);
                duration[i] = this.duration(a, name);
                duration[i].SetInitialValue(a.Reference.Durations.Maximum.TotalMinutes);
                location[i] = this.location(a, name);
                //Extra variable denoting if the activity is included in the plan
                schedulable[i] = this.schedulable(a, name);
                schedulable[i].SetInitialValue(1);

                model.AddDecisions(start[i], duration[i], location[i], schedulable[i]);
                // Assumed they are ordered
                int max = Settings.getInt(a.Reference.TemporalDomain.Last().End);
                
                model.AddConstraints("Deadline" + name, start[i] + duration[i] <= max);

                value += addValue(i, a.Reference.Value.Importance, name);

                i++;
                if (i == length)
                {
                    break;
                }
            }
            model.AddGoal("Value", GoalKind.Maximize, value);


            nonOverlappingConstraint(model, start, duration, location, schedulable);

            Console.WriteLine("Be patient... This may take quite (a lot) of time...");
            solution = context.Solve();
            Console.WriteLine("Finished");
        }


        private Decision start(Activity a, string name)
        {
            Decision d = new Decision(
                Domain.IntegerRange(
                    Settings.getInt(a.Reference.TemporalDomain.First().Start),
                    Settings.getInt(a.Reference.TemporalDomain.Last().End)),
                  "Start" + name);

            return d;
        }

        private Decision duration(Activity a, string name)
        {
            return new Decision(
                Domain.IntegerRange(
                    a.Reference.Durations.Minimum.TotalMinutes,
                    a.Reference.Durations.Maximum.TotalMinutes),
                "Duration" + name);
        }

        private Decision location(Activity a, string name)
        {
            return new Decision(

                Domain.IntegerRange(0, a.Reference.Locations.Count-1),
                "Location" + name);
        }

        private Decision schedulable(Activity a, string name)
        {
            return new Decision(Domain.IntegerRange(0, 1), "Schedulable" + name);
        }

        private string addValue(int index, int priority, string name)
        {
            if (index < length - 1)
            {
                return " ((Duration" + name + ") * " + priority + " * Schedulable" + name + ")+";
            }
            else
            {
                return " ((Duration" + name + ") * " + priority + " * Schedulable" + name + ")";
            }
        }

        private void nonOverlappingConstraint(Model model, Decision[] start, Decision[] duration, Decision[] location, Decision[] schedulable)
        {
            for (int j = 0; j < length; j++)
            {
                for (int k = 0; k < length; k++)
                {
                    // Non overlapping cannot occur for the same activity
                    if (k == j)
                    {
                        break;
                    }
                    model.AddConstraint(
                        string.Format("NonOverlapping{0}{1}", j, k),
                        Model.Or(
                            Model.Or(Model.Not(schedulable[j]), Model.Not(schedulable[k])),
                            Model.Or(start[k] + duration[k] <= start[j], start[k] >= start[j] + duration[j])));
                }
            }
        }


        public DateTime getDate(int i)
        {
            return Settings.StartDate.AddMinutes(i);
        }


        public override void Initialize()
        {
            length = Activities.Count;
        }
    }
}
