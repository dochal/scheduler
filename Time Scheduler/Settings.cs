﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Time_Scheduler
{
    /// <summary>
    /// The main class containing the neccessery fir
    /// </summary>
    public class Settings
    {
        /// <summary>
        /// Defines the date from the scheduling should begin
        /// </summary>
        public readonly static DateTime StartDate = new DateTime(2015, 09, 29, 18, 00, 00);

        /// <summary>
        /// Optinal (necessery for some algorithms) the date when the scheduling can end
        /// </summary>
        public readonly static DateTime EndDate = new DateTime(2016, 09, 30, 23, 59, 00);

        /// <summary>
        /// New random object doesn't create a new random sequence therefore it needs to
        /// be initialized as a singleton
        /// </summary>
        public static Random Random = new Random();

        /// <summary>
        /// The maximum number from which the random can
        /// </summary>
        public readonly static int MAX = 10000000;

        /// <summary>
        /// Sets if the bike is availble to the user
        /// </summary>
        public readonly static bool Bike = true;

        /// <summary>
        /// Sets if teh car is available to the user
        /// </summary>
        public readonly static bool Car = true;

        /// <summary>
        /// Sets if the user is willing to the the public transport
        /// </summary>
        public readonly static bool Transit = true;

        public static int getInt(DateTime t)
        {
            int x = (int)(t - StartDate).TotalMinutes;
            return x < 0 ? 0 : x;
        }
    }
}
