﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Time_Scheduler.Components
{
    /// <summary>
    /// Represents possible time difference.
    /// </summary>
    public class Range
    {
        /// <summary>
        /// Returns the maximum value
        /// </summary>
        public TimeSpan Minimum;

        /// <summary>
        /// Return the minimum value
        /// </summary>
        public TimeSpan Maximum;


        public Range(TimeSpan min, TimeSpan max)
        {
            this.Minimum = min;
            this.Maximum = max;
        }

        /// <summary>
        /// Inclusive on both ends
        /// </summary>
        /// <param name="value">The time value to check</param>
        /// <returns>True if timespan is in the range</returns>
        public bool InRange(TimeSpan value)
        {
            return (value >= Minimum && value <= Maximum) ? true : false;
        }
    }
}
