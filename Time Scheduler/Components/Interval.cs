﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Time_Scheduler.Components
{
    /// <summary>
    /// Denotes allowable time between two specific dates
    /// </summary>
    public class Interval
    {
        /// <summary>
        /// The start date of the interval
        /// </summary>
        public DateTime Start;

        /// <summary>
        /// The end date of the interval
        /// </summary>
        public DateTime End;

        /// <summary>
        /// Returns the time difference between End and Start
        /// </summary>
        public TimeSpan Difference
        {
            get
            {
                return End - Start;
            }
        }

        /// <summary>
        /// Creates a new interval object with a specifict start and end time
        /// </summary>
        /// <param name="start">Represents the time beginning</param>
        /// <param name="end">Represents the time end</param>
        public Interval(DateTime start, DateTime end)
        {
            this.Start = start;
            this.End = end;
        }

        /// <summary>
        /// Checks if the datetime is occurs anywhere within the interval
        /// </summary>
        /// <param name="value">The datetime to check</param>
        /// <returns>True if exists</returns>
        public bool InInterval(DateTime value)
        {
            return value >= Start && value <= End ? true : false;
        }

        /// <summary>
        /// Checks if another interval is with range
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public bool Fits(Interval interval)
        {
            return interval.Start >= Start && interval.End <= End ? true : false;
        }
        
    }
}
