﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Time_Scheduler.Algorithms;
using Time_Scheduler.Models;
using Time_Scheduler.Models.Items;
using Time_Scheduler.Models.Travel;

namespace Time_Scheduler
{
    /// <summary>
    /// The main scheduling class
    /// </summary>
    public class Scheduler
    {
        
        /// <summary>
        /// Creates a new instance of the scheduler
        /// </summary>
        public Scheduler(Sample items, Map map) {
            Console.WriteLine("This sample has: {0}", items.Value);
            for (int i = 100; i <= 700; i += 100)
            {
                Console.WriteLine("------------------------------------------------");
                Console.WriteLine("Initial Population: {0}:", i);
                Algorithm x = new Algorithms.Permutation(this.getActivities(items), map, i) { } ;
                x.Initialize();
                x.Execute();
                OutputData(items.Count, i, items.Value - x.Best().Value);
            }
        }

        public List<Activity> getActivities(List<Schedulable> items)
        {
            items = items.OrderBy(a => Guid.NewGuid()).ToList();
            List<Activity> activities = new List<Activity>();
            foreach (Schedulable item in items)
            {
                if (item.Interruptible)
                {
                    double x = Math.Ceiling((double)(item.TotalTime.TotalMinutes / item.Durations.Minimum.TotalMinutes));
                    for (int i = 0; i <= x; i++)
                    {
                        activities.Add(new Activity(item));
                    }
                }
                else
                {
                   activities.Add(new Activity(item));
                }
            }
            return activities;
        }

        public void Output(Plan activities)
        {
            DDay.iCal.iCalendar iCal = new DDay.iCal.iCalendar()
            {
                Method = "PUBLISH",
                Version = "2.0"
            };
            foreach (Activity a in activities)
            {
                if (a.Reference != null)
                {
                    if (a.End - a.Begin > TimeSpan.FromSeconds(0))
                    {
                        var travel = iCal.Create<DDay.iCal.Event>();
                        travel.Start = new DDay.iCal.iCalDateTime(a.Begin);
                        travel.End = new DDay.iCal.iCalDateTime(a.End);
                        travel.Summary = "Travelling by " + a.Transport;
                    }
                    var evt = iCal.Create<DDay.iCal.Event>();
                    evt.Start = new DDay.iCal.iCalDateTime(a.Start);
                    evt.End = new DDay.iCal.iCalDateTime(a.Finish);
                    evt.Summary = a.Reference.Name;
                    evt.Location = a.Location.Name;
                }
            }

            DDay.iCal.Serialization.iCalendar.iCalendarSerializer serializer = new DDay.iCal.Serialization.iCalendar.iCalendarSerializer();
            string output = serializer.SerializeToString(iCal);
            System.IO.File.WriteAllText(@"Output.ics", output);
            Console.WriteLine("Printed the Output");
        }

        public void OutputData(int sampleSize, int population, Value value)
        {
            using (StreamWriter sw = File.AppendText("results.txt"))
            {
                sw.WriteLine(string.Format("{0},{1},{2},{3},{4},{5}", sampleSize, population, value.Importance, value.Health, value.Finance, value.Enjoyment));
            }
        }
    }
    
}
